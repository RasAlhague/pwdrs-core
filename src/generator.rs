
use rand::Rng;
use serde::{Deserialize, Serialize};

pub struct PwdGenerator {
    has_no_digits: bool,
    has_no_uppercase: bool,
    has_no_special_chars: bool,
    length: u32,
}

impl PwdGenerator {
    fn generate_default_pwd(&self, chars: &[char]) -> String {
        let mut pwd: Vec<char> = Vec::new();
        let mut rng = rand::thread_rng();

        for _i in 0..self.length {
            pwd.push(chars[rng.gen_range(0..chars.len())]);
        }

        pwd.into_iter().collect()
    }

    fn generate_with_chars(&self, source_pwd: &str, divider: u32, chars: &[char]) -> String {
        let mut pwd: Vec<char> = source_pwd.chars().collect();
        let mut rng = rand::thread_rng();

        let change_count = self.length / divider;

        for _i in 0..change_count {
            let random_index = rng.gen_range(0..self.length);
            pwd[random_index as usize] = chars[rng.gen_range(0..chars.len())];
        }

        pwd.into_iter().collect()
    }

    fn get_divider(&self) -> u32 {
        let mut divider = 4;

        if self.has_no_digits {
            divider -= 1;
        }
        if self.has_no_special_chars {
            divider -= 1;
        }
        if self.has_no_uppercase {
            divider -= 1;
        }

        divider
    }
}

impl PwdGenerator {
    #[must_use]
    pub fn new_with(
        has_no_digits: bool,
        has_no_uppercase: bool,
        has_no_special_chars: bool,
        length: u32,
    ) -> PwdGenerator {
        PwdGenerator {
            has_no_digits,
            has_no_special_chars,
            has_no_uppercase,
            length,
        }
    }

    #[must_use]
    pub fn generate(&self, char_collection: &CharCollection) -> String {
        let mut pwd = self.generate_default_pwd(&char_collection.lowercase_chars);
        let divider = self.get_divider();

        if !self.has_no_digits {
            pwd = self.generate_with_chars(&pwd, divider, &char_collection.digit_chars);
        }
        if !self.has_no_uppercase {
            pwd = self.generate_with_chars(&pwd, divider, &char_collection.uppercase_chars);
        }
        if !self.has_no_special_chars {
            pwd = self.generate_with_chars(&pwd, divider, &char_collection.special_chars);
        }

        pwd
    }
}

/// Contains all the lists with chars for different password.
#[derive(Serialize, Deserialize)]
pub struct CharCollection {
    /// List of chars for default password generation
    pub lowercase_chars: Vec<char>,
    /// list of chars for password with uppercased chars
    pub uppercase_chars: Vec<char>,
    /// list of chars for password with digits
    pub digit_chars: Vec<char>,
    /// list of chars for passwords with special chars
    pub special_chars: Vec<char>,
}

impl CharCollection {
    #[must_use]
    /// Returns an `CharCollection` with default charakters.
    pub fn default() -> CharCollection {
        CharCollection {
            lowercase_chars: vec![
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
                'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            ],
            uppercase_chars: vec![
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            ],
            special_chars: vec![
                '!', '"', '§', '$', '%', '&', '/', '(', ')', '=', '?', '#', '<', '>', ',', '.',
                '_', '+', '-', '*',
            ],
            digit_chars: vec!['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'],
        }
    }

    /// Returns an `CharCollection` loaded from a json file.
    ///
    /// # Arguments
    /// * `file_path` - The path to the json file which should be loaded.
    /// 
    /// # Errors
    /// If deserialization fails an error will be thrown.
    pub fn from_json(content: &str) -> Result<CharCollection, String> {
        let char_collection: CharCollection = match serde_json::from_str(&content) {
            Ok(c) => c,
            Err(err) => {
                println!("Err: {}", err);

                return Err(String::from("Error at deserilizing json!"));
            }
        };

        Ok(char_collection)
    }

    #[must_use]
    pub fn to_json(&self) -> String {
        serde_json::to_string(self).unwrap()
    }
}